vim.g.mapleader = " "

-- Save and quit
vim.keymap.set("n", "<leader>w", "<cmd>w<CR>")
vim.keymap.set("n", "<leader>q", "<cmd>confirm q<cr>")

-- Splits
vim.keymap.set("n", "|", "<cmd>vsplit<CR>")
vim.keymap.set("n", "\\", "<cmd>split<CR>")

-- Windows
vim.keymap.set("n", "<c-k>", "<cmd>wincmd k<CR>")
vim.keymap.set("n", "<c-j>", "<cmd>wincmd j<CR>")
vim.keymap.set("n", "<c-h>", "<cmd>wincmd h<CR>")
vim.keymap.set("n", "<c-l>", "<cmd>wincmd l<CR>")

-- Buffers
vim.keymap.set("n", "]b", "<cmd>BufferLineCycleNext<CR>")
vim.keymap.set("n", "[b", "<cmd>BufferLineCyclePrev<CR>")
vim.keymap.set("n", "<leader>x", "<cmd>bdelete!<CR>")

-- NeoTree
vim.keymap.set("n", "<leader>e", "<cmd>Neotree toggle<CR>")

-- Comment
-- vim.keymap.set("n", "<leader>/", ":CommentToggle<CR>")
-- vim.keymap.set("x", "<leader>/", "<cmd>'<,'>CommentToggle<CR>")

-- Search
vim.keymap.set("n", "<leader>h", ":nohlsearch<CR>")

-- Terminal
vim.keymap.set("n", "<leader>tf", ":ToggleTerm direction=float<CR>")
vim.keymap.set("n", "<leader>th", ":ToggleTerm direction=horizontal<CR>")
vim.keymap.set("n", "<leader>tv", ":ToggleTerm direction=vertical size=40<CR>")
