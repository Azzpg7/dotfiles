vim.opt.tabstop = 2
vim.opt.shiftwidth = 2

vim.keymap.set(
	"n",
	"<leader>r",
	[[:w<cr>:TermExec cmd="clear && clang++ -g -fsanitize=address -std=c++23 -Wall -Wextra -Wshadow -DONPC -O2 %:p -o %:p:r && %:p:r"<cr>]],
	{ silent = true, buffer = true }
)
